Fractal
-------

Implements a custom Direct2D effect that draws the Mandelbrot set on the GPU. The displayed image can be zoomed with the mouse wheel and scrolled by dragging while the left mouse button is down, both of which are forwarded as effect parameters.

![Fracal1](https://www.dropbox.com/s/45oe15f3jx9tekn/Fractal1.png?raw=1)
![Fracal2](https://www.dropbox.com/s/b9h98g3w3j3q37v/Fractal2.png?raw=1)
![Fracal3](https://www.dropbox.com/s/1hjbeo7bgml3iaq/Fractal3.png?raw=1)
![Fracal4](https://www.dropbox.com/s/t1bqwer4raedz25/Fractal4.png?raw=1)

Holographic
-----------

Uses Direct3D to render a model with a hologram-like shader effect.

The backdrop is a starfield that's generated in a pixel shader by sampling a noise function based on normal distribution.

[Video Demo](https://www.dropbox.com/s/0lmd3suzg15ti5u/Holographic.mp4?dl=0)
