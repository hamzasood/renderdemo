// Backdrop_VS.hlsl
// Generates a screen-aligned quad.

struct VertexShaderOutput {
   float2 texCoord : TEXCOORD;
   float4 position : SV_POSITION;
};

VertexShaderOutput main(uint id : SV_VERTEXID)
{
   VertexShaderOutput output;

   // Map from the vertex id to UV-space.
   // This forms the following triangle strip:
   //
   // v1 - v4
   // |  \  |
   // v0 - v2
   output.texCoord = float2(id & 1, id >> 1);

   // Calculate the position by mapping from UV-space to clip-space.
   output.position = float4(
      2 * output.texCoord.x - 1,
      1 - 2 * output.texCoord.y,
      0.9999999f,
      1.0f);

   return output;
}
