#pragma once

#include <d3d11_1.h>

#include <Shared/ComHelpers.h>
#include <Shared/ComponentRenderer.h>

namespace renderdemo::holographic {

class BackdropComponent : public IRenderComponent {
public:
   void onRendererStartup(ID3D11Device1*) override;

   void render(ID3D11DeviceContext1*,
      const StepTimer&, const D3D11_VIEWPORT& screenViewport) override;

   void onRendererShutdown() override;

private:
   ComPtr<ID3D11VertexShader> m_vertexShader;
   ComPtr<ID3D11PixelShader> m_pixelShader;
   ComPtr<ID3D11ShaderResourceView> m_noiseTexture;
   ComPtr<ID3D11SamplerState> m_noiseSamplerState;
};

} // namespace renderdemo::holographic
