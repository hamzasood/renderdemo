#include "pch.h"
#include "BackdropComponent.h"

#include <Shared/ComHelpers.h>
#include <Shared/DDSTextureLoader.h>
#include <Shared/EmbeddedData.h>

#include "Resource.h"

using namespace DirectX;

namespace renderdemo::holographic {

namespace {
   auto createBackdropVertexShader(
      ID3D11Device* device, ID3D11VertexShader** outShader) -> HRESULT
   {
      const auto [p, sz] = findEmbeddedData(MAKEINTRESOURCE(IDR_BACKDROP_VS));
      return device->CreateVertexShader(p, sz, nullptr, outShader);
   }

   auto createStarsPixelShader(
      ID3D11Device* device, ID3D11PixelShader** outShader) -> HRESULT
   {
      const auto [p, sz] = findEmbeddedData(MAKEINTRESOURCE(IDR_STARS_PS));
      return device->CreatePixelShader(p, sz, nullptr, outShader);
   }

   auto loadNoiseTexture(
      ID3D11Device* device, ID3D11ShaderResourceView** outTexture) -> HRESULT
   {
      const auto [p, sz] = findEmbeddedData(MAKEINTRESOURCE(IDR_NOISE_DDS));

      return CreateDDSTextureFromMemory(
         device,
         static_cast<const std::uint8_t*>(p),
         sz,
         nullptr,
         outTexture);
   }

   auto createNoiseSampler(
      ID3D11Device* device, ID3D11SamplerState** outSampler) -> HRESULT
   {
      auto desc = D3D11_SAMPLER_DESC{};
      desc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
      desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
      desc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
      desc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;

      return device->CreateSamplerState(&desc, outSampler);
   }
}

void BackdropComponent::onRendererStartup(ID3D11Device1* device)
{
   VALIDATE_HR(createBackdropVertexShader(device, &m_vertexShader));
   VALIDATE_HR(createStarsPixelShader(device, &m_pixelShader));
   VALIDATE_HR(loadNoiseTexture(device, &m_noiseTexture));
   VALIDATE_HR(createNoiseSampler(device, &m_noiseSamplerState));
}

void BackdropComponent::render(
   ID3D11DeviceContext1* deviceCtx,
   const StepTimer& /* timer */, const D3D11_VIEWPORT& /* screenViewport */)
{
   // Bind shaders.
   deviceCtx->VSSetShader(m_vertexShader, nullptr, 0);
   deviceCtx->PSSetShader(m_pixelShader, nullptr, 0);

   // Bind the noise texture.
   deviceCtx->PSSetShaderResources(0, 1, &m_noiseTexture.GetInterfacePtr());
   deviceCtx->PSSetSamplers(0, 1, &m_noiseSamplerState.GetInterfacePtr());

   // Draw a single quad without any source data.
   // The vertex shader will generate the appropriate data.
   deviceCtx->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
   deviceCtx->IASetVertexBuffers(0, 0, nullptr, nullptr, nullptr);
   deviceCtx->IASetInputLayout(nullptr);
   deviceCtx->Draw(4, 0);
}

void BackdropComponent::onRendererShutdown()
{
   m_noiseSamplerState.Release();
   m_noiseTexture.Release();
   m_pixelShader.Release();
   m_vertexShader.Release();
}

} // namespace renderdemo::holographic
