static const float4 baseColor = {0.2f, 0.2f, 0.65f, 0.8f};

cbuffer cbPerFrame {
   float3 cameraPos;
   float time;
};

/// Generate a square wave that alternates between 0 and 1.
float squareWave(float x)
{
   return floor(2 * x) - 2 * floor(x);
}

/*------------------------------------------------------------------------------
   Lighting
   Implements the Phong Reflection Model
   See: https://en.wikipedia.org/wiki/Phong_reflection_model#Description

   The calculations are done in world space, even though it would be slightly
   simpler in camera space. This is because the hologram effect is applied in
   world space, and it's easier to do everything in the same co-ordinate space.
------------------------------------------------------------------------------*/

static const float3 lightDir = normalize(float3(0.0f, 0.0f, 1.0f));

static const float specularIntensity = 0.5f;
static const float shininess = 16.0f;

float diffuse(float3 normal)
{
   return saturate(dot(normal, -lightDir));
}

float specular(float3 position, float3 normal)
{
   const float3 R = reflect(lightDir, normal);
   const float3 V = normalize(cameraPos - position);
   return pow(saturate(dot(R, V)), shininess) * specularIntensity;
}

float phongIllumination(float3 position, float3 normal)
{
   return diffuse(normal) + specular(position, normal);
}

/*------------------------------------------------------------------------------
   Hologram Effect
------------------------------------------------------------------------------*/

static const float barFrequency = 50.0f;
static const float barScrollSpeed = 0.04f;
static const float barAlphaFactor = 0.5f;

float hologramBars(float3 position)
{
   // 0 -> No bars.
   // 1 -> Bars.
   const float gate = squareWave(
      barFrequency * (position.y + barScrollSpeed * time));

   return 1 - (1 - barAlphaFactor) * gate;
}

/*----------------------------------------------------------------------------*/

float4 main(
   float3 posWorld : POSITION,
   float3 normalWorld : NORMAL) : SV_TARGET
{
   float4 result = baseColor;

   // Normalise the normal vector.
   normalWorld = normalize(normalWorld);

   // Apply lighting.
   result.rgb *= phongIllumination(posWorld, normalWorld);

   // Apply hologram.
   result.a *= hologramBars(posWorld);

   return result;
}
