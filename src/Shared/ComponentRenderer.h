#pragma once

#include <Windows.h>
#include <d3d11_1.h>

#include <atomic>
#include <cassert>
#include <memory>
#include <thread>
#include <vector>

namespace renderdemo {

class StepTimer;

class IRenderComponent {
public:
   /// Called before entering the render loop.
   virtual void onRendererStartup(ID3D11Device1*) = 0;

   /// Render a single frame.
   virtual void render(ID3D11DeviceContext1*,
      const StepTimer&, const D3D11_VIEWPORT& screenViewport) = 0;

   /// Called after exiting the render loop.
   virtual void onRendererShutdown() = 0;
};

/// Manages the rendering of independent component objects.
/// A component is an object derived from IRenderComponent.
class ComponentRenderer {
public:
   ComponentRenderer() = default;

   ~ComponentRenderer()
   {
      assert(!isRunning()
         && "The renderer is being destroyed while it's still running. "
            "This probably means that the window is outliving the renderer.");
   }

   /// Whether the rendering has started.
   auto isRunning() const -> bool
   {
      return m_renderThread.joinable();
   }

   /// Add a renderable component.
   /// To reduce on the required synchronisation, this is only possible before
   /// starting up the renderer.
   void addComponent(std::shared_ptr<IRenderComponent> component)
   {
      assert(!isRunning() && "Can't add components while running");
      m_components.emplace_back(std::move(component));
   }

   /// Start rendering to the given window.
   /// The renderer will stay active until the window is destroyed; you must
   /// ensure that the renderer object isn't destructed before then.
   void runAsync(HWND outputWindow);

private:
   /// Messages for communicating with the render thread.
   enum class Message {
      /// Null value; do nothing.
      None,

      /// The output window is resizing.
      Resize,

      /// The output window is being destroyed.
      ShutDown,
   };

   /// Subclass callback for the output window.
   static auto CALLBACK windowSubclassProc(
      HWND window, UINT msg, WPARAM, LPARAM,
      UINT_PTR idSubclass, DWORD_PTR refData) noexcept -> LRESULT;

   /// Entry point for the render thread.
   void renderThreadMain(HWND outputWindow);

   std::thread m_renderThread;
   std::vector<std::shared_ptr<IRenderComponent>> m_components;
   std::atomic<Message> m_message = Message::None;
};

} // namespace renderdemo
