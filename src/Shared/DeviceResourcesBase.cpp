#include "pch.h"
#include "DeviceResourcesBase.h"

#include <d3d11_1.h>
#include <dxgi1_2.h>

#include <cassert>
#include <utility>

#include "ComHelpers.h"

namespace renderdemo {

namespace {
   /// Minimum Direct3D feature level that's assumed throughout this codebase.
   constexpr auto minFeatureLevel = D3D_FEATURE_LEVEL_11_1;

   /// Base set of flags to initialise every device with.
   constexpr auto baseDeviceFlags = 0u
#ifdef _DEBUG
      | D3D11_CREATE_DEVICE_DEBUG
#endif
      | D3D11_CREATE_DEVICE_BGRA_SUPPORT;

   /// Find the factory that was used to create the given device instance.
   auto findDXGIFactory(ID3D11Device* device) -> ComPtr<IDXGIFactory2>
   {
      auto factory = ComPtr<IDXGIFactory2>{};

      auto dxgiDevice = ComPtr<IDXGIDevice>{device};
      assert(dxgiDevice && "Failed to cast a D3D device to a DXGI device");

      auto adapter = ComPtr<IDXGIAdapter>{};
      VALIDATE_HR(dxgiDevice->GetAdapter(&adapter));

      // The factory is the parent of the device's adapter.
      // See: https://docs.microsoft.com/en-us/windows/desktop/api/dxgi/nn-dxgi-idxgifactory#remarks
      VALIDATE_HR(adapter->GetParent(IID_PPV_ARGS(&factory)));

      return factory;
   }
}

auto drbase::createDevice()
   -> std::pair<ComPtr<ID3D11Device>, ComPtr<ID3D11DeviceContext>>
{
   auto device = ComPtr<ID3D11Device>{};
   auto deviceContext = ComPtr<ID3D11DeviceContext>{};

   VALIDATE_HR(
      D3D11CreateDevice(
         /* pAdapter */ nullptr,
         /* DriverType */ D3D_DRIVER_TYPE_HARDWARE,
         /* Software */ nullptr,
         /* Flags */ baseDeviceFlags,
         /* pFeatureLevels */ &minFeatureLevel,
         /* FeatureLevels */ 1,
         /* SDKVersion */ D3D11_SDK_VERSION,
         /* ppDevice */ &device,
         /* pFeatureLevel */ nullptr,
         /* ppImmediateContext */ &deviceContext));

   return {device, deviceContext};
}

auto drbase::createSwapChain(ID3D11Device* device, HWND outputWindow)
   -> ComPtr<IDXGISwapChain1>
{
   auto swapChain = ComPtr<IDXGISwapChain1>{};

   auto desc = DXGI_SWAP_CHAIN_DESC1{};
   desc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
   desc.SampleDesc.Count = 1;
   desc.SampleDesc.Quality = 0;
   desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
   desc.BufferCount = 2;
   desc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;

   VALIDATE_HR(
      findDXGIFactory(device)->CreateSwapChainForHwnd(
         /* pDevice */ device,
         /* hWnd */ outputWindow,
         /* pDesc */ &desc,
         /* pFullscreenDesc */ nullptr,
         /* pRestrictToOutput */ nullptr,
         /* ppSwapChain */ &swapChain));

   return swapChain;
}

} // namespace renderdemo
