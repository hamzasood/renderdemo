#pragma once

#include <iostream>
#include <streambuf>

namespace renderdemo {

/// Input buffer that reads directly from a chunk of memory.
/// This is essentially a stringbuf that doesn't allocate or copy.
class MemoryInBuffer final : public std::streambuf {
public:
   MemoryInBuffer(const void* data, std::size_t dataSize)
   {
      // Position the get area over the whole of the input data.
      // We have to cast away the const to use the streambuf interface, but that
      // should be safe as long as we don't actually modify the data.
      const auto first = const_cast<char*>(static_cast<const char*>(data));
      setg(first, first, first + dataSize);
   }

protected:
   auto showmanyc() -> std::streamsize override
   {
      // Because the get area covers all of the data, the number of characters
      // remaining in the source data is the same as in the get area.
      return egptr() - gptr();
   }

   auto seekoff(off_type, std::ios::seekdir,
      std::ios::openmode = std::ios::in) -> pos_type override;
   auto seekpos(pos_type,
      std::ios::openmode = std::ios::in) -> pos_type override;
};

/// Stream interface for a MemoryInBuffer.
class MemoryInStream final : public std::istream {
public:
   MemoryInStream(const void* data, std::size_t dataSize)
      : std::istream{&m_buffer}
      , m_buffer{data, dataSize}
   {
   }

   ~MemoryInStream() override;

private:
   MemoryInBuffer m_buffer;
};

} // namespace renderdemo
