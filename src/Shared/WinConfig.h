// WinConfig.h
// This file defines various macros to help rein in the Windows API header.
#pragma once

// Exclude rarely used API declarations.
#define WIN32_LEAN_AND_MEAN

// Exclude unneeded modules.
#define NOGDI
#define NOHELP
#define NOMCX
#define NOSERVICE

// Suppress the min/max macro definitions.
#define NOMINMAX
