#pragma once

#include "WinConfig.h"
#include <Windows.h>

#include <d2d1_1.h>
#include <d3d11_1.h>
#include <dxgi1_2.h>

#include <comdef.h>

#include <array>
#include <atomic>
#include <iostream>
#include <memory>
#include <optional>
#include <stdexcept>
#include <thread>
#include <type_traits>
#include <vector>
