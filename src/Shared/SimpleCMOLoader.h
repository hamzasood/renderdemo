#pragma once

#include "CMOSpec.h"

#include <array>
#include <iosfwd>
#include <stdexcept>
#include <vector>

namespace renderdemo {

/// Encapsulates vertex and index data.
struct CompiledMeshData {
   struct Vertex : VSD3DStarter::Vertex {
      /// Descriptions for the vertex structure elements.
      static const std::array<D3D11_INPUT_ELEMENT_DESC, 5> elementDescs;
   };

   std::vector<Vertex> vertices;
   std::vector<std::uint16_t> indices;
};

/// Exception that's thrown if the input data uses any features of the CMO
/// format that our simple parser doesn't support.
class CMOTooComplex : public std::runtime_error {
public:
   using std::runtime_error::runtime_error;
   ~CMOTooComplex() override;
};

/// Parse a (very simple) compiled mesh object.
/// If the input isn't simple enough, a CMOTooComplex exception is thrown.
///
/// An object is considered simple if:
///   1. It contains a single mesh.
///   2. The mesh is comprised of a single sub-mesh.
///   3. The sub-mesh doesn't have a material applied to it.
///   4. There's no skinning or skeletal animations.
auto loadMeshFromCMO(std::istream&) -> CompiledMeshData;

} // namespace renderdemo
