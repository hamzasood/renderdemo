// ComHelpers.h
// Helpers for working with the Common Object Model.
#pragma once

#include <comdef.h>
#include <objbase.h>

#include <cassert>

namespace renderdemo {

// Define a macro for checking the result of COM functions.
#ifndef NDEBUG
#define VALIDATE_HR(expr) assert(SUCCEEDED(expr))
#else
#define VALIDATE_HR ::_com_util::CheckError
#endif

/// RAII initialisation for the COM library.
class ComInitializer {
public:
   explicit ComInitializer(DWORD coInit = COINIT_APARTMENTTHREADED)
   {
      VALIDATE_HR(
         CoInitializeEx(nullptr, coInit));
   }

   ComInitializer(const ComInitializer&) = delete;
   auto operator=(const ComInitializer&) = delete;

   ~ComInitializer()
   {
      CoUninitialize();
   }
};

/// COM smart pointer type.
/// The alias template allows for easy use of _com_ptr_t without having to worry
/// about per-interface typedefs or IIID parameters.
template <typename T>
using ComPtr = _com_ptr_t<_com_IIID<T, &__uuidof(T)>>;

} // namespace renderdemo
