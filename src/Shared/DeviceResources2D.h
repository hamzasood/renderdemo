#pragma once

#include <d2d1_1.h>

#include "ComHelpers.h"
#include "DeviceResourcesBase.h"

namespace renderdemo {

/// Manages resources required for drawing with Direct2D.
class DeviceResources2D : public DeviceResourcesBase<DeviceResources2D> {
public:
   explicit DeviceResources2D(HWND outputWindow)
   {
      initBase(outputWindow);
   }

   auto factory() const -> ID2D1Factory1* { return m_factory; }
   auto device() const -> ID2D1Device* { return m_device; }
   auto deviceContext() const -> ID2D1DeviceContext* { return m_deviceContext; }
   using DeviceResourcesBase::swapChain;
   auto backBufferBitmap() const -> ID2D1Bitmap1* { return m_backBufferBitmap; }

private:
   friend DeviceResourcesBase;

   void createDeviceIndependentResources();
   void createDeviceDependentResources();
   void createWindowSizeDependentResources();
   void releaseWindowSizeDependentResources();

   ComPtr<ID2D1Factory1> m_factory;
   ComPtr<ID2D1Device> m_device;
   ComPtr<ID2D1DeviceContext> m_deviceContext;
   ComPtr<ID2D1Bitmap1> m_backBufferBitmap;
};

} // namespace renderdemo
