#pragma once

#include <d3d11_1.h>

#include "ComHelpers.h"
#include "DeviceResourcesBase.h"

namespace renderdemo {

/// Manages resources required for drawing with Direct3D.
class DeviceResources3D : public DeviceResourcesBase<DeviceResources3D> {
public:
   explicit DeviceResources3D(HWND outputWindow)
   {
      initBase(outputWindow);
   }

   auto device() const -> ID3D11Device1* { return d3dDevice(); }
   auto deviceContext() const -> ID3D11DeviceContext1* { return d3dDeviceContext(); }
   using DeviceResourcesBase::swapChain;
   auto renderTargetView() -> ID3D11RenderTargetView* { return m_renderTargetView; }
   auto depthStencilView() -> ID3D11DepthStencilView* { return m_depthStencilView; }
   auto screenViewport() -> const D3D11_VIEWPORT& { return m_screenViewport; }

private:
   friend DeviceResourcesBase;

   void createWindowSizeDependentResources();
   void releaseWindowSizeDependentResources();

   ComPtr<ID3D11RenderTargetView> m_renderTargetView;
   ComPtr<ID3D11DepthStencilView> m_depthStencilView;
   D3D11_VIEWPORT m_screenViewport = {};
};

} // namespace renderdemo
