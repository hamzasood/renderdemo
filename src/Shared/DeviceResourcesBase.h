#pragma once

#include <d3d11_1.h>
#include <dxgi1_2.h>

#include <cassert>
#include <tuple>
#include <utility>

#include "ComHelpers.h"

namespace renderdemo {

/// Common base class for the DeviceResources types.
///
/// This class manages a Direct3D device and swap chain, and uses CRTP to
/// provide a set of overridable methods that help a derived class manage
/// resources of its own.
///
/// Note that two-phase initialisation is required; see initBase for details.
template <typename Derived>
class DeviceResourcesBase {
public:
   /// Update the held resources after a window resize operation.
   /// This will resize buffers etc. to match the new size.
   void handleWindowSizeChanged();

protected:
   /// Finish initialisation of the object.
   /// This can't be done in a constructor because it invokes callbacks on the
   /// derived object, which won't work properly until it's fully constructed.
   void initBase(HWND outputWindow);

   //
   // Overridable Methods
   //

   /// Create resources that don't depend on the device in any way.
   void createDeviceIndependentResources() {}

   /// Create resources that're tied to a particular device.
   void createDeviceDependentResources() {}

   /// Create resources that'll need re-creating whenever the window resizes.
   void createWindowSizeDependentResources() {}

   /// Release resources that were created in createWindowSizeDependentResources.
   /// Omitting the release of any such resource here will most likely cause the
   /// swap chain resize to fail.
   void releaseWindowSizeDependentResources() {}

   //
   // Accessors
   // These are protected to allow the derived class to decide what's exposed.
   //

   auto* d3dDevice() const { return m_device.GetInterfacePtr(); }
   auto* d3dDeviceContext() const { return m_deviceContext.GetInterfacePtr(); }
   auto* swapChain() const { return m_swapChain.GetInterfacePtr(); }

private:
   auto& getDerived() { return *static_cast<Derived*>(this); }
   auto& getDerived() const { return *static_cast<const Derived*>(this); }

   ComPtr<ID3D11Device1> m_device;
   ComPtr<ID3D11DeviceContext1> m_deviceContext;
   ComPtr<IDXGISwapChain1> m_swapChain;
};

/*------------------------------------------------------------------------------
   Inline Implementation
------------------------------------------------------------------------------*/

// Private functions that aren't dependent on the template parameter, factored
// out the reduce the amount of templated code.
namespace drbase {
   auto createDevice()
      -> std::pair<ComPtr<ID3D11Device>, ComPtr<ID3D11DeviceContext>>;

   auto createSwapChain(ID3D11Device*, HWND outputWindow)
      -> ComPtr<IDXGISwapChain1>;
}

template <typename Derived>
void DeviceResourcesBase<Derived>::initBase(HWND outputWindow)
{
   assert(!m_device && "Already initialised");

   getDerived().createDeviceIndependentResources();
   std::tie(m_device, m_deviceContext) = drbase::createDevice();
   getDerived().createDeviceDependentResources();
   m_swapChain = drbase::createSwapChain(m_device, outputWindow);
   getDerived().createWindowSizeDependentResources();
}

template <typename Derived>
void DeviceResourcesBase<Derived>::handleWindowSizeChanged()
{
   getDerived().releaseWindowSizeDependentResources();

   // Resize the swap chain buffers to fill the new client area.
   VALIDATE_HR(
      m_swapChain->ResizeBuffers(0, 0, 0, DXGI_FORMAT_UNKNOWN, 0));

   getDerived().createWindowSizeDependentResources();
}

} // namespace renderdemo
