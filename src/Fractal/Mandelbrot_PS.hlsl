#define D2D_INPUT_COUNT 0
#define D2D_REQUIRES_SCENE_POSITION
#include <d2d1effecthelpers.hlsli>

/// Maximum greyscale colour value to use in the image.
/// This is clamped because pure white stands out a bit too much.
static const float maxIntensity = 0.75f;

// Effect parameters.
// See MandelbrotEffect.h for more info on these.
cbuffer params {
   float2 outputSize;
   double scale;
   double2 center;
};

/// Number of iterations of the sequence to perform before giving up and
/// assuming that the number is part of the set.
static const int MAX_ITERATIONS = 250;

/// Square the given vector as if it were a complex number.
double2 squareComplex(double2 z)
{
   // (x + yi)^2 = (x^2 - y^2) + 2xyi
   return double2(z.x * z.x - z.y * z.y, 2 * z.x * z.y);
}

/// Determine whether the given number is definitely not a member of the set.
/// If this returns true, then no further iteration is necessary.
bool hasEscaped(double2 z)
{
   // A property of the Mandelbrot set is that if the magnitude of the number
   // exceeds 2, then it'll eventually diverge and hence isn't part of the set.
   // See: https://en.wikipedia.org/wiki/Mandelbrot_set#Basic_properties
   return (z.x * z.x + z.y * z.y) > 4;
}

/// Calculates the normalised iteration count for an escaped value.
/// This is used to reduce the appearance of bands when colouring the image.
float normalizedIter(int i, float2 z)
{
   // The algorithm is described in detail here:
   // https://en.wikipedia.org/wiki/Mandelbrot_set#Continuous_(smooth)_coloring

   const float log_zn = log(z.x * z.x + z.y * z.y) / 2;
   const float log_2 = log(2);

   return i + 1 - log(log_zn / log_2) / log_2;
}

D2D_PS_ENTRY(main)
{
   const float2 pos = D2DGetScenePosition().xy;
   const float aspectRatio = outputSize.x / outputSize.y;

   // Convert the pixel position to a point on the complex plane.
   const double2 c
      = center
      + scale * float2(aspectRatio, 1) * ((pos / outputSize) - 0.5);

   // Evaluate the sequence.
   double2 z = c;
   for (int i = 0; i < MAX_ITERATIONS; ++i) {
      // f(z) = z^2 + c
      z = squareComplex(z) + c;

      if (hasEscaped(z)) {
         // Colour the point based on how fast the value escaped.
         // A linear scale contains too many dark colours, so the square root is
         // used to ramp up the brightness faster at lower values.
         const float val
            = maxIntensity
            * sqrt(normalizedIter(i, (float2)z) / MAX_ITERATIONS);

         return float4(val, val, val, 1.0f);
      }
   }

   return 0;
}
